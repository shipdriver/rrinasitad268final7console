﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace RRinas_ITAD268_Final_Q4
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\CalculateRate.exe";
            pa.StartInfo.Arguments = @"100000 10 5";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();
            
            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");
            
        }

        [TestMethod]
        public void TestMethod2()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\CalculateRate.exe";
            pa.StartInfo.Arguments = @"100000 10 5";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");

        }

        [TestMethod]
        public void TestMethod3()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\CalculateRate.exe";
            pa.StartInfo.Arguments = @"100000 10 5";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");

        }

        [TestMethod]
        public void TestMethod4()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\CalculateRate.exe";
            pa.StartInfo.Arguments = @"100000 10 5";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");

        }

        [TestMethod]
        public void TestMethod5()
        {
            Process pa = new Process();
            pa.StartInfo.FileName = @"C:\Test\CalculateRate.exe";
            pa.StartInfo.Arguments = @"100000 10 5";
            pa.StartInfo.UseShellExecute = false;
            pa.StartInfo.RedirectStandardOutput = true;
            pa.Start();
            pa.WaitForExit();

            string result = pa.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "1,060.66\r\n");

        }
    }
}